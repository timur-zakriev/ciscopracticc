#include <iostream>
#include <string>
#include <sstream>
using namespace std;

int main(){
	string str;
	while(cout << "String => " && getline(cin, str) && ! str.empty()){
		cout << "Result => ";
		istringstream ist(str);
		string prev = "";
		while ( ist >> str ){
			if ( str != prev ){
				cout << str << ' ';
				prev = str;
			}
		}
		cout << endl;
	}
	return 0;
}
