#include <iostream>
#include <string>
using namespace std;

int main(){
  string first, second;
  bool anagram = false;
  getline(cin, first);
  getline(cin, second);

  for(int i = 0; i < first.length(); i++){
    if(second.find(first[i]) < second.length()){
      anagram = true;
    } else {
      anagram = false;
    }
  }
  
  anagram ? cout << "anagrams\n" : cout << "NOT anagrams\n";
  return 0;
}
