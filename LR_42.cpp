#include <iostream>
#include <cmath>
using namespace std;
int increment(int &x, int y=1) {
	return x += y;
}
int increment(float &x, int y=1) {
	return x += y;
}
int main() {
  int intvar = 0;
  float floatvar = 1.5;
  for(int i = 0; i < 10; i++) {
    if(i % 2) {
      increment(intvar);
      increment(floatvar, sqrt(intvar));
    } else {
      increment(intvar,i);
      increment(floatvar);
    }
  }
  cout << intvar * floatvar << endl;
  return 0;
}
