#include <iostream>
using namespace std;

int main() {
  int year;
  cout << "Input a year => "; cin >> year;
  if ((year % 4 != 0) || ((year % 100 == 0) && (year % 400 != 0))){
    cout << "Common year\n";
  } else {
    cout << "leap year\n";
  }
  return 0;
}
