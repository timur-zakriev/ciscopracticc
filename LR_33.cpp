#include <iostream>
using namespace std;

int main (){
  int vector[] = {3, -5, 7, 10, -4, 14, 5, 2, -13};
  int n = sizeof(vector) / sizeof(vector[0]);
  int* curs = vector, min = *curs;
  for (int i = 0; i < n; i++){
    if(min > *(curs + i)){
      min = *(curs + i);
    }
  }
  cout << "Min element vector = " << min << endl;

  return 0;
}
