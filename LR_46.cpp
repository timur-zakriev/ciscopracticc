#include <iostream>
using namespace std;
int sum(int n){
  return (n > 1) ? n + sum(n - 1): n;
}
int main(){
  int n;
  cout << "Input N => ";  cin >> n;
  cout << sum(n) << endl;
  return 0;
}
