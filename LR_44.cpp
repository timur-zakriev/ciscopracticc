#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
struct Collection {
	int elno;
	int *elements;
};
void AddToCollection(Collection &col, int element) {
	if (col.elno == 0) {
		col.elno++;
		col.elements = new int[col.elno];
		col.elements[col.elno] = element;
	} else {
		int *dop;
		dop = new int[col.elno+1];
		for (int i = 0; i<col.elno; i++) {
			dop[i] = col.elements[i];
		}
		dop[col.elno] = element;
		delete[]col.elements;
		col.elements = new int[col.elno + 1];
		for (int i = 0; i<col.elno+1; i++) {
			col.elements[i] = dop[i];
		}
		delete[]dop;
		col.elno++;
	}
}
void PrintCollection(Collection col) {
	cout << "[ ";
	for(int i = 0; i < col.elno; i++) {
		cout << col.elements[i] << " ";
	}
	cout << "]" << endl;
}
int main() {
	Collection collection = { 0, NULL };
	int elems;
	cout << "How many elements? ";
	cin >> elems;
	srand(time(NULL));
	for(int i = 0; i < elems; i++) {
		AddToCollection(collection, rand() % 100 + 1);
	}
	PrintCollection(collection);
	delete[] collection.elements;
	return 0;
}
