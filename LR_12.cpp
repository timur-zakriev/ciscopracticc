#include <iostream>

using namespace std;

int main()
{
  int year, month, day;

  cout << "Input a year => ";   cin >> year;
  cout << "Input a month => ";  cin >> month;
  cout << "Input a day => ";    cin >> day;

  month -= 2;
  if (month < 0){
    month += 12;
    year--;
  }
  month = (month * 83) / 32;
  month += day;
  month += year;
  month += year / 4;
  month -= year / 100;
  month += year / 400;
  month %= 7;

  cout << month << endl;

  return 0;
}
