#include <iostream>
using namespace std;
struct Date {
	int year;
	int month;
	int day;
};
bool isLeap(int year) {
	if (year % 4) {
		 return false;
	} else if (year % 100) {
		 return true;
	} else if (year % 400) {
		 return false;
	} else {
		return true;
	}
}
int monthLength(int year, int month) {
	switch(month)
	{
		case 1: return 31; break;
		case 2: return isLeap(year)? 29:28; break;
		case 3: return 31; break;
		case 4: return 30; break;
		case 5: return 31; break;
		case 6: return 30; break;
		case 7: return 31; break;
		case 8: return 31; break;
		case 9: return 30; break;
		case 10: return 31; break;
		case 11: return 30; break;
		case 12: return 31; break;
	}
}
int dayOfYear(Date date) {
	int days = 0;
	for (int i = 1; i < date.month; i++) {
		days += monthLength(date.year, i);
	}
	days += date.day;
	return days;
}
int daysBetween (Date start, Date end) {
	int days = dayOfYear(end) - dayOfYear(start);
	while (start.year < end.year) {
		days += isLeap(start.year) ? 366 : 365;
		start.year++;
	}
	return (days>=0) ? days : -1;
}
int main(void) {
	Date d, t;
	cout << "Enter start year month day: ";
	cin >> d.year >> d.month >> d.day;
	cout << "Enter end year month day: ";
	cin >> t.year >> t.month >> t.day;
	cout << daysBetween (d, t) << endl;
	return 0;
}
