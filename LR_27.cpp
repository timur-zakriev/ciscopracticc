#include <iostream>
using namespace std;

int main(){
  int value;
  cout << "Input valuse => "; cin >> value;
  if (value > 0){
    while (value != 0){
      if (value / 50 > 0){
        value -= 50;
        cout << "50 ";
      } else if (value / 20 > 0){
        value -= 20;
        cout << "20 ";
      } else if (value / 10 > 0){
        value -= 10;
        cout << "10 ";
      } else if (value / 5 > 0) {
        value -= 5;
        cout << "5 ";
      } else{
        value -= 1;
        cout << "1 ";
      }
    }
  } else {
    cout << "Wrong value :(\n";
    return 0;
  }
  cout << endl;
  return 0;
}
