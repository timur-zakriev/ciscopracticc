#include <iostream>
#include <cmath>
using namespace std;

bool is_close(double a, double b, double tolerance){
  return fabs(a - b) < tolerance ? true : false;
}
int main(){
  if (0.3 == 3 * 0.1) {
    cout << "The numbers are equal\n";
  } else {
    cout << "The numbers are not equal\n";
  }

  if (is_close(0.3, 3 * 0.1, 0.00000001)) {
    cout << "The numbers are close enough\n";
  } else {
    cout << "The numbers are not close enough\n";
  }

  if (is_close(3 * 0.1, 0.3, 0.00000001)){
    cout << "The numbers are still close enough\n";
  } else {
    cout << "The numbers are not close enough\n";
  }

  if (is_close(3 * 0.1, 0.31, 0.00000001)){
    cout << "The numbers are still close enough\n";
  } else {
    cout << "The numbers are not close enough\n";
  }

  return 0;
}
