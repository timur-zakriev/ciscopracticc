#include <iostream>
#include <cmath>
using namespace std;

int main(){
  double vector[] = { 1., 2., 3., 4., 5. };
  int n = sizeof(vector) / sizeof(vector[0]);
  double ArithmeticMean, HarmonicMean, GeometricMean = 1, RootMeanSquare;

  for(int i = 0; i < n; i++){
  	ArithmeticMean += vector[i] / n;
  	HarmonicMean += 1 / vector[i];
  	GeometricMean *= vector[i];
  	RootMeanSquare += pow(vector[i],2);
  }
  HarmonicMean = n / HarmonicMean;
  GeometricMean = pow(GeometricMean,1. / n);
  RootMeanSquare = sqrt(RootMeanSquare / n);

  cout << "Arithmetic Mean = " << ArithmeticMean << endl;
  cout << "Harmonic Mean = " << HarmonicMean << endl;
  cout << "Geometric Mean = " << GeometricMean << endl;
  cout << "RootMean Square = " << RootMeanSquare << endl;

  return 0;
}
