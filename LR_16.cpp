#include <iostream>
using namespace std;

int main(){
  unsigned long long power, result = 2;
  cout << "Input power of 2 => "; cin >> power;
  
  if (power <= 0 || power > 63){
    cout << "Wrong input :(" << endl;
  } else {
    for (int i = 1; i < power; i++){
      result *= 2;
    }
    cout << result << endl;
  }

  return 0;
}
