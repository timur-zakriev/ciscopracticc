#include <iostream>
using namespace std;

int main(){
  long int result = 1;
  int n;
  cout << "Input number => "; cin >> n;

  if (n >= 0){
    for (int i = 1; i <= n; i++) {
      result = result * i;
    }
    cout << result << endl;
  } else {
    cout << "Wrong input :(\nNumber can't be negative.\n";
  }

  return 0;
}
