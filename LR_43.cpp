#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int main() {
	int maxball;
	int ballsno;

	cout << "Max ball number? ";
	cin >> maxball;
	cout << "How many balls? ";
	cin >> ballsno;

	srand(time(NULL));
	int *array;
	array = new int[ballsno];
	cout<<"Balls:\n";

	for(int i = 0; i < ballsno; i++) {
		array[i] = rand() % (maxball + 1);
		cout << array[i] << " ";
	}

	delete[]array;
	return 0;
}
