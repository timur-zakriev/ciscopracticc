#include <iostream>
#include <time.h>
using namespace std;
int main(){
  struct clock{
    int hoursStart;
    int minutesStart;
    int hoursEnd;
    int minutesEnd;
    int hoursResult;
    int minutesResult;
  }clock;

  cout << "Input time start =>";
  cin >> clock.hoursStart >> clock.minutesStart;
  if(clock.hoursStart < 0 || clock.minutesStart < 0){
    cout << "Time can't be negative!\n";
    return 0;
  }
  cout << "Input time end =>";
  cin >> clock.hoursEnd >> clock.minutesEnd;
  if(clock.hoursEnd < 0 || clock.minutesEnd < 0){
    cout << "Time can't be negative!\n";
    return 0;
  }
  clock.hoursResult = clock.hoursEnd - clock.hoursStart;
  clock.minutesResult = clock.minutesEnd - clock.minutesStart;
  if (clock.minutesResult < 0) {
    clock.hoursResult--;
    clock.minutesResult += 60;
  }

  cout << clock.hoursResult << ":" << clock.minutesResult << endl;

  return 0;
}
