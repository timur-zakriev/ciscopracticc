#include <iostream>
using namespace std;
int main(){
  unsigned long long a ,b, n;
  a = 0;
  b = 1;
  cout << "Input n => "; cin >> n;
  while (n <= 0){
    cout << "Error. Enter the number greater than zero => "; cin >> n;
  }
  while (n != 0){
    a += b;
    b = a - b;
    n -= 1;
  }
  cout << a << endl;
  return 0;
}
