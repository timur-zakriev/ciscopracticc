#include <iostream>
#include <string>
#include <stdlib.h>
using namespace std;
int main() {
	string s, t;
	int n, it = 0, c = 0, maxValue = 0;
	bool correct = true, onlyDigitsAndDots = true;
	cin >> s;
	for(int i = 0; i < s.length(); i++) {
		n = -1;
		if(s[i] != '.') {
			t.resize(it + 1);
			t[it++] = s[i];
			if((s[i] < 48 || s[i] > 57) && (s[i] != 46)) {
				onlyDigitsAndDots = false;
			}
		}
		else {
			c++;
			n = atoi(t.c_str());
			t.clear();
			it = 0;
			maxValue = (n > maxValue ? n : maxValue);
			if(n < 0 || n > 255)
				correct = false;
		}
	}
	n = atoi(t.c_str());
	if(onlyDigitsAndDots) {
		if(c == 3) {
			if(correct) {
				cout << "Correct IP";
			}
			else {
				if(maxValue >= 1000) {
					cout << "Too many characters in a part";
				}
				else {
					cout << "Too big (or small) a value of a part";
				}
			}
		}
		else if(c > 3) {
			cout << "Too many parts";
		}
		else {
			cout << "Incorrect parts count";
		}
	}
	else {
		cout << "Only digits and dots allowed";
	}
	return 0;
}
